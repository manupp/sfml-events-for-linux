# Eventos

En esta práctica escribirás un programa gráfico con movimientos y eventos (del teclado y del ratón).

## Resultado

Se trata de construir un programa que produzca el siguiente resultado:

![Vid](./events.mp4)

<span style="font-size:8">Click icon by <a href="https://freeicons.io/profile/714">Raj Dev</a> on <a href="https://freeicons.io">freeicons.io</a></span>

Observa que:

1. El avión se mueve hacia arriba y hacia abajo al pulsar las teclas "Flecha arriba" y "Flecha abajo", respectivamente.
2. El avión no pasa de los bordes superior e inferior de la ventana.
2. El fondo se mueve continuamente hacia la izquierda (para dar la impresión de que el avión está avanzando hacia la derecha).
3. Al hacer clic con el ratón en cualquier parte de la pantalla, tanto el avión como el fondo se detienen, y aparece el texto "PAUSED" en la pantalla. Al hacer clic otra vez, el movimiento se reanuda, y desaparece el texto.

## Tutorial

Tienes que crear los siguientes objetos:
- Ventana (tamaño: 1200 x 900). Tipo: `Window`.
- Imagen de fondo (nombre del archivo: `assets/background.png`). Tipo: `Sprite`.
- Avión (nombre del archivo: `assets/plane.png`). Tipo: `Sprite`.
- Texto `PAUSE`. Tipo: `Text`.

Además, necesitarás una variable para saber si el programa está o no pausado (tipo: `bool`). A este tipo de variables tipo `bool`, que nos dicen cuál es el estado del programa, las llamamos *variables de estado*.

### Cómo hacer que el avión se mueva con las teclas

En la sección *Actualización*, comprueba si está pulsada la tecla correspondiente (lee en los apuntes de Schoology cómo hacerlo), y, si lo está, mueve el avión con el método `move`. Tendrás que ajustar la velocidad con la que se mueve hacia arriba y hacia abajo, desplazando el avión la cantidad adecuada. Ten en cuenta que ese método `move` se ejecutará en cada fotograma (mientras esté pulsada la tecla), y que se dibujan al menos 25 fotogramas cada segundo. Por ejemplo, si usas `move(0, 1)`, en cada fotograma, el avión se desplazará un píxel hacia abajo, y, por tanto, en un segundo, el avión bajará 25 píxeles.

### Cómo hacer que el avión se mueva fuera de los bordes de la ventana

Las sentencias `move` descritas en el apartado anterior solo se ejecutan si la posición del avión está dentro de los bordes de la ventana. Es decir, hay que crear una sentencia `if` para que las sentencias anteriores solo se ejecuten en ese caso. Solo es necesario comprobar la coordenada vertical (`getPosition().y`).

### Cómo hacer que el fondo se desplace continuamente

En realidad, lo que hacemos es crear dos fondos (tipo `Sprite`), y colocar uno a continuación del otro.

Luego, en cada fotograma, desplazamos los dos fondos a la vez (usando el método `move`), hacia la izquierda. Tendrás que ajustar la velocidad.

En algún momento, el fondo de la izquierda saldrá de la ventana por el borde izquierdo. Tenemos que detectar (con una sentencia `if`) que ha ocurrido eso, y, en ese momento, recolocamos el fondo que acaba de salir, poniéndolo justo detrás del segundo.

Con el segundo fondo, hacemos lo mismo.

Para ello, necesitaremos dos sentencias `if` que comprueben la posición de cada fondo en cada fotograma.

### Cómo hacer que el movimiento se detenga

Al hacer clic con el ratón en cualquier sitio, el movimiento (tanto del fondo como del avión) se detiene.

Esto lo controlamos en la sección de *Gestión de evento*. Detectamos el evento correspondiente a la pulsación del botón del ratón. En caso de detectarlo, comprobamos si el botón pulsado es el izquierdo. En caso afirmativo, lo único que hacemos es cambiar la variable de estado correspondiente. Si es `false`, la ponemos a `true`; si es `true`, a `false`. La forma más sencilla de hacer eso es así (suponiendo que la variable se llama `paused`):

``` c++
paused = !paused;
```

Luego, en la sección de *Actualización*, todo el código se mete dentro de una sentencia `if (!paused)`, de modo que solo se ejecute si el estado de `paused` es `false`.

Además, en la sección de *Dibujo*, incluimos una sentencia `if (paused)`, de modo que el texto se dibuja solo si el estado de `paused` es `true`.

                                
